from django import forms

class UploadFileForm(forms.Form):
    file = forms.FileField()

class ScanStatusForm(forms.Form):
    statuscode = forms.IntegerField()
    scan_id = forms.UUIDField()
    text = forms.CharField(max_length=20000)
