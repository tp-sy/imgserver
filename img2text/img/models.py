import uuid
import json
from enum import IntEnum
from django.db import models


class Statuscodes(IntEnum):
    FAILED = -1
    IN_PROGRESS = 0
    SUCCESS = 1
    TIMED_OUT = 2
    

class ScanResult(models.Model):
    scan_id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    statuscode = models.IntegerField()
    text = models.CharField(max_length=20000, default="")
    start_time = models.IntegerField(default=0)

    def as_json(self):
        return json.dumps({
            "status": Statuscodes(int(self.statuscode)).name,
            "scan_id": str(self.scan_id),
            "text": str(self.text)
        }, indent=4)