from django.shortcuts import get_object_or_404
from django.http import HttpResponse, HttpResponseBadRequest
from .forms import UploadFileForm, ScanStatusForm
from .produce import publish_message
from .models import ScanResult, Statuscodes
from django.views.decorators.csrf import csrf_exempt
import time

@csrf_exempt
def new_scan(request):
    # TODO/FIXME: Proper returncodes
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            scanres = ScanResult(statuscode=0, start_time=time.time())
            scanres.save()
            publish_message(request.FILES['file'].read(), uuid=str(scanres.scan_id))
            return HttpResponse(str(scanres.scan_id))
    else:
        form = UploadFileForm()
    return HttpResponseBadRequest("Failed to make a new scan")
    
@csrf_exempt
def scan_status(request):
    if request.method == 'POST':
        form = ScanStatusForm(request.POST)
        if form.is_valid():
            scan_id = form.cleaned_data["scan_id"]
            statuscode = form.cleaned_data["statuscode"]
            scanres = get_object_or_404(ScanResult, pk=scan_id)
            scanres.statuscode = statuscode
            scanres.text = form.cleaned_data["text"]
            scanres.save()
            return HttpResponse("Updated scan result")
    else:
        ScanStatusForm()
    return HttpResponseBadRequest("Something went wrong")

@csrf_exempt
def get_scan_status(request, scan_id):
    scanres = get_object_or_404(ScanResult, pk=scan_id)
    if time.time() - scanres.start_time > 600 and int(scanres.statuscode) == Statuscodes.IN_PROGRESS.value: # abort after 10 minutes
        scanres.statuscode = Statuscodes.TIMED_OUT.value
        scanres.save()
    return HttpResponse(scanres.as_json())