import os
import sys
from kombu import Connection, Exchange, Producer
from dotenv import load_dotenv

load_dotenv()

# Create the connection

def publish_message(msg, uuid):
    # Read config values
    routing_key = os.environ.get("ROUTING_KEY", "img")
    exchange_name = os.environ.get("EXCHANGE", "img_exchange")
    rabbit_address = os.environ.get("RABBIT", "amqp://localhost:8081/")

    conn = Connection(rabbit_address)
    channel = conn.channel()
    test_exchange = Exchange(exchange_name, type="direct")
    producer = Producer(exchange=test_exchange, channel=channel,
                        routing_key=routing_key)

    producer.publish(msg, headers={"uuid": uuid})

if __name__ == "__main__":
    fname = sys.argv[1]
    with open(fname, "rb") as fd:
        image = fd.read(-1)
    publish_message(image, 0)
    