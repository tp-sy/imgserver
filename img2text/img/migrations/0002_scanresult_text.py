# Generated by Django 4.0.3 on 2022-03-13 12:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('img', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='scanresult',
            name='text',
            field=models.CharField(default='', max_length=20000),
        ),
    ]
