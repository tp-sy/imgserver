from django.urls import path

from . import views

urlpatterns = [
    path('new_scan/', views.new_scan, name='new_scan'),
    path('scan_status/', views.scan_status, name='scan_status'),
    path('scan_status/<uuid:scan_id>/', views.get_scan_status, name='get_scan_status')
]
