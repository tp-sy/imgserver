FROM debian:11

WORKDIR /app
RUN apt update
RUN apt install -y python3 python3-dev python3-pip
COPY img2text .
COPY requirements.txt .
RUN pip3 install -r requirements.txt
RUN python3 manage.py makemigrations
RUN python3 manage.py migrate
CMD ["python3", "manage.py", "runserver"]